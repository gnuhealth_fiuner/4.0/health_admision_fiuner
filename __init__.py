from trytond.pool import Pool

from . import health_admision_fiuner

from .wizard import sign_admision

def register():
    Pool.register(
        health_admision_fiuner.PatientAdmision,
        sign_admision.SignAdmisionStart,
        module='health_admision_fiuner', type_='model')
    Pool.register(
        sign_admision.SignAdmisionWizard,
        module='health_admision_fiuner', type_='wizard')
