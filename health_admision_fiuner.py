from trytond.model import ModelView, ModelSQL, fields
from trytond.pool import Pool
from trytond.rpc import RPC
from trytond.pyson import Eval, Not, Bool, Equal, Or
from trytond.transaction import Transaction
from trytond.modules.health.core import (get_institution, compute_age_from_dates,
                   get_health_professional)
from trytond.modules.health_crypto.health_crypto import HealthCrypto

import hashlib
import json


class PatientAdmision(ModelView, ModelSQL):
    'Health admision'
    __name__ = 'gnuhealth.patient.admision'

    name = fields.Char('Name',
                        states={'readonly': Eval('state').in_(['partially_signed', 'signed'])})
    patient = fields.Many2One('gnuhealth.patient', 'Patient', required=True,
                        states={'readonly': Eval('state').in_(['partially_signed', 'signed'])})
    observations = fields.Text('Observations',
                        states={'readonly': Eval('state').in_(['partially_signed', 'signed'])})
    start_date = fields.DateTime('Admision date',
                        states={'readonly': Eval('state').in_(['partially_signed', 'signed'])})
    initiated_by = fields.Many2One('gnuhealth.healthprofessional', 'Initiated_by',
                        #readonly=True)
                        )
    ### Health prof 1 sign
    signed_by1 = fields.Many2One('gnuhealth.healthprofessional', 'Signed by (1)',
                        readonly=True)
    digital_signature1 = fields.Text('Digital Signature (1)', readonly=True)
    serializer1 = fields.Text('Doc String (1)', readonly=True)
    serializer_current1 = fields.Function(
        fields.Text('Current Doc (1)',
                    states={
                            'invisible': Not(Bool(Eval('digest_status1'))),
                            }),
        'check_digest')
    document_digest1 = fields.Char('Digest 1', readonly=True,
                                help="Original Document Digest")
    digest_current1 = fields.Function(
        fields.Char('Current Hash (1)',
                    states={
                            'invisible': Not(Bool(Eval('digest_status1'))),
                            }),
        'check_digest')
    digest_status1 = fields.Function(
        fields.Boolean('Altered (1)',
                       states={
                        'invisible': Not(Eval('state').in_(['partially_signed', 'signed'])),
                        },
                       help="This field will be set whenever parts of"
                            " the main original document has been changed."
                            " Please note that the verification is done"
                            " only on selected fields."),
        'check_digest')

    ### Health prof 2 sign
    signed_by2 = fields.Many2One('gnuhealth.healthprofessional', 'Signed by (2)',
                        readonly=True)
    digital_signature2 = fields.Text('Digital Signature (2)', readonly=True)
    serializer2 = fields.Text('Doc String (2)', readonly=True)
    serializer_current2 = fields.Function(
        fields.Text('Current Doc (2)',
                    states={
                            'invisible': Not(Bool(Eval('digest_status2'))),
                            }),
        'check_digest')
    document_digest2 = fields.Char('Digest (2)', readonly=True,
                                help="Original Document Digest")
    digest_current2 = fields.Function(
        fields.Char('Current Hash (2)',
                    states={
                            'invisible': Not(Bool(Eval('digest_status2'))),
                            }),
        'check_digest')
    digest_status2 = fields.Function(
        fields.Boolean('Altered (2)',
                       states={
                        'invisible': Not(Eval('state').in_(['partially_signed', 'signed'])),
                        },
                       help="This field will be set whenever parts of"
                            " the main original document has been changed."
                            " Please note that the verification is done"
                            " only on selected fields."),
        'check_digest')

    ### Health prof 3 sign
    signed_by3 = fields.Many2One('gnuhealth.healthprofessional', 'Signed by (3)',
                        readonly=True)
    digital_signature3 = fields.Text('Digital Signature (3)', readonly=True)
    serializer3 = fields.Text('Doc String (3)', readonly=True)
    serializer_current3 = fields.Function(
        fields.Text('Current Doc (3)',
                    states={
                            'invisible': Not(Bool(Eval('digest_status3'))),
                            }),
        'check_digest')
    document_digest3 = fields.Char('Digest (3)', readonly=True,
                                help="Original Document Digest")
    digest_current3 = fields.Function(
        fields.Char('Current Hash (3)',
                    states={
                            'invisible': Not(Bool(Eval('digest_status3'))),
                            }),
        'check_digest')
    digest_status3 = fields.Function(
        fields.Boolean('Altered (3)',
                       states={
                        'invisible': Not(Eval('state').in_(['partially_signed', 'signed'])),
                        },
                       help="This field will be set whenever parts of"
                            " the main original document has been changed."
                            " Please note that the verification is done"
                            " only on selected fields."),
        'check_digest')

    state = fields.Selection([
        ('draft', 'Draft'),
        ('partially_signed', 'Partialy signed'),
        ('signed', 'Signed')
        ], 'State', sort=False, readonly=True)

    @staticmethod
    def default_initiated_by():
        return get_health_professional()

    @staticmethod
    def default_state():
        return 'draft'

    @classmethod
    @ModelView.button_action('health_admision_fiuner.act_gnuhealth_sign_admision_wizard')
    def sign(cls, admisions):
        pass

    @classmethod
    def __setup__(cls):
        super().__setup__()
        cls._buttons.update({
                'sign': {
                    'invisible': Equal(Eval('state'), 'signed'),
                },
            })
        ''' Allow calling the set_signature method via RPC '''
        cls.__rpc__.update({
                'set_signature': RPC(readonly=False),
                })

    @classmethod
    def get_serial1(cls, admision, hp):
        pool = Pool()
        HP = pool.get('gnuhealth.healthprofessional')
        hp = HP(hp)
        admision = admision[0]

        data_to_serialize = {
            'Patient': str(admision.patient.rec_name) or '',
            'Start': str(admision.start_date) or '',
            'Initiated_by': str(admision.initiated_by.rec_name),
            'Signed_by1': hp.rec_name,
            'observations': admision.observations or ''
             }
        serialized_doc = str(HealthCrypto().serialize(data_to_serialize))

        return serialized_doc

    @classmethod
    def get_serial2(cls, admision, hp):
        admision = admision[0]

        data_to_serialize = {
            'serialiazer1': admision.serializer_current1,
            'Signed_by2': hp.rec_name or '',
             }

        serialized_doc = str(HealthCrypto().serialize(data_to_serialize))

        return serialized_doc

    @classmethod
    def get_serial3(cls, admision, hp):
        admision = admision[0]

        data_to_serialize = {
            'document_digest': admision.serializer_current2,
            'Signed_by3': hp.rec_name,
             }

        serialized_doc = str(HealthCrypto().serialize(data_to_serialize))

        return serialized_doc


    def check_digest(self, name):
        result = ''
        serial_doc = ''
        # digest_status
        if (name == 'digest_status1' and self.document_digest1):
            serial_doc = str(self.get_serial1([self], self.signed_by1))
            if (HealthCrypto().gen_hash(serial_doc) == self.document_digest1):
                result = False
            else:
                ''' Return true if the document has been altered'''
                result = True
        if (name == 'digest_status2' and self.document_digest2):
            serial_doc = str(self.get_serial2([self], self.signed_by2))
            if (HealthCrypto().gen_hash(serial_doc) == self.document_digest2):
                result = False
            else:
                ''' Return true if the document has been altered'''
                result = True
        if (name == 'digest_status3' and self.document_digest3):
            serial_doc = str(self.get_serial3([self], self.signed_by3))
            if (HealthCrypto().gen_hash(serial_doc) == self.document_digest3):
                result = False
            else:
                ''' Return true if the document has been altered'''
                result = True

        # digest_current
        if name == 'digest_current1' and self.signed_by1:
            serial_doc = str(self.get_serial1([self], self.signed_by1))
            result = HealthCrypto().gen_hash(serial_doc)
        if name == 'digest_current2' and self.signed_by2:
            serial_doc = str(self.get_serial2([self], self.signed_by2))
            result = HealthCrypto().gen_hash(serial_doc)
        if name== 'digest_current3' and self.signed_by3:
            serial_doc = str(self.get_serial3([self], self.signed_by3))
            result = HealthCrypto().gen_hash(serial_doc)

        # serializer
        if name == 'serializer_current1' and self.signed_by1:
            serial_doc = str(self.get_serial1([self], self.signed_by1))
            result = serial_doc
        if name == 'serializer_current2' and self.signed_by2:
            serial_doc = str(self.get_serial2([self], self.signed_by2))
            result = serial_doc
        if name == 'serializer_current3' and self.signed_by3:
            serial_doc = str(self.get_serial3([self], self.signed_by3))
            result = serial_doc

        return result

    #@classmethod
    #def view_attributes(cls):
        #return [('//group[@id="ds1"]', 'states', {
                #'invisible': Not(Eval('state') == 'sign'),
                #})] + \
                #[('//group[@id="ds2"]', 'states', {
                #'invisible': Not(Eval('state') == 'sign'),
                #})] + \
                #[('//group[@id="ds3"]', 'states', {
                #'invisible': Not(Eval('state') == 'sign'),
                #})]


