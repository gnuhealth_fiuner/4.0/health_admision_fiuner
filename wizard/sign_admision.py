from trytond.model import ModelView, fields
from trytond.wizard import Wizard, Button, StateView, StateTransition
from trytond.pool import Pool
from trytond.transaction import Transaction
from trytond.exceptions import UserError
from trytond.modules.health_crypto.health_crypto import HealthCrypto

class SignAdmisionStart(ModelView):
    'Sign Admision - Start'
    __name__ = 'gnuhealth.patient.sign_admision.start'

    user = fields.Char("User", required=True)
    password =  fields.Char("Password", required=True)

    @staticmethod
    def default_password():
        return 'x' * 10


class SignAdmisionWizard(Wizard):
    'Sign Admision - Wizard'
    __name__ = 'gnuhealth.patient.sign_admision.wizard'

    start = StateView('gnuhealth.patient.sign_admision.start',
            'health_admision_fiuner.gnuhealth_patient_sign_admision_start_view',[
                    Button('Cancel', 'end', 'tryton-cancel'),
                    Button('Ok', 'check_sign', 'tryton-ok', default=True)
                    ])

    check_sign = StateTransition()

    def transition_check_sign(self):
        pool = Pool()
        User = pool.get('res.user')
        Admision = pool.get('gnuhealth.patient.admision')
        HP = pool.get('gnuhealth.healthprofessional')

        admision = Admision(Transaction().context.get('active_id'))
        login = self.start.user
        password = self.start.password
        hp = HP.search([('name.internal_user.login', '=', login)])

        if hp:
            if User.get_login(login, {'password': password}):
                if not admision.signed_by1:
                    serial_doc = Admision.get_serial1([admision], hp[0].id)
                    admision.signed_by1 = hp[0]
                    admision.state = 'partially_signed'
                    admision.save()
                    admision.serializer1 = serial_doc
                    admision.document_digest1 = HealthCrypto().gen_hash(serial_doc)
                    admision.save()
                elif not admision.signed_by2:
                    admision.signed_by2 = hp[0]
                    admision.save()
                    serial_doc = Admision.get_serial2([admision], hp[0])
                    admision.serializer2 = serial_doc
                    admision.document_digest2 = HealthCrypto().gen_hash(serial_doc)
                    admision.save()
                elif not admision.signed_by3:
                    admision.signed_by3 = hp[0]
                    admision.save()
                    serial_doc = Admision.get_serial3([admision], hp[0])
                    admision.serializer3 = serial_doc
                    admision.document_digest3 = HealthCrypto().gen_hash(serial_doc)
                    admision.state = 'signed'
                    admision.save()
                else:
                    raise UserError('admision already signed')
            else:
                raise UserError('wrong user or password')
        else:
            raise UserError('not a registered health professional')

        return 'end'
